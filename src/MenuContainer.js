import React, {Component} from "react";
import MenuButton from "./MenuButton";
import Menu from "./Menu";
import "./blackground.css";

class MenuContainer extends Component{
     constructor(props){
         super(props);
         this.state={
             visible:false
         };
         this.toggleMenu=this.toggleMenu.bind(this);
          this.handleMouseDown = this.handleMouseDown.bind(this);
     }
   handleMouseDown(e){
       this.toggleMenu();
       e.stopPropagation();
   }
    toggleMenu(){

        this.setState({
           visible:!this.state.visible
        });

    }
    render(){
        let show__menu;
        if(this.state.visible==true) {show__menu="open";}

        return(
            <div className={show__menu}>
<MenuButton handleMouseDown={this.handleMouseDown} />
            <Menu handleMouseDown={this.handleMouseDown} menuVisability={this.state.visible} />
            <div>
        <p>Найдёшь пункt, который здесь лишний?</p>
            <ul>
                <li>Aenean</li>
                <li>Lorem</li>
                <li>Ipsum</li>
                <li>Dolor</li>
                <li>Sit</li>
                <li>Bumblebees</li>
                <li>Consectetur</li>
            </ul>
            </div>
            </div>
        );
    }
}
export default MenuContainer;
