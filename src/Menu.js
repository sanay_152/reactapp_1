import React, {Component} from "react";
import "./Menu.css";

class Menu extends Component{
    render(){
        let visibility = "hide";
        if (this.props.menuVisability){
            visibility = "show";
        }
        return(
            <div id="flyoutMenu" onMouseDown={this.props.handleMouseDown} className={visibility}>
            <h2><a href="#">Главная</a></h2>
            <h2><a href="#">О</a></h2>
            <h2><a href="#">Контакты</a></h2>
            <h2><a href="#">Поиск</a></h2>
            <div className="under__block">
            <div className="plank"></div>
            <p>все права защищены</p>
            <p>sanay_152</p>
            </div>
            </div>

        );
    }
}

  export default Menu;
